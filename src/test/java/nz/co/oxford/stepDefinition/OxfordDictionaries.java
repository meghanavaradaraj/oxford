package nz.co.oxford.stepDefinition;

import com.cucumber.listener.Reporter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import nz.co.oxford.helpers.Log;
import nz.co.oxford.modelObjects.OxfordDictionariesAPIDetails;
import nz.co.oxford.modules.OxfordDictionariesAPI;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import static nz.co.oxford.helpers.ExcelUtil.*;
import static nz.co.oxford.helpers.Hooks.prop;

public class OxfordDictionaries {
    List<String> headWordId = new ArrayList<String>();
    List<String> meaning = new ArrayList<String>();
    List<String> inputWord = new ArrayList<String>();
    int numberOfExecutions;
    OxfordDictionariesAPI oxfordDictionariesAPI = new OxfordDictionariesAPI();

    @When("^the oxforddictionaries API returns a response of (\\d+)$")
    public void theOxforddictionariesAPIReturnsAResponseOf(int statusCode) {
        for (int i=0; i<numberOfExecutions; i++) {
            OxfordDictionariesAPIDetails oxfordDictionariesAPIDetails = new OxfordDictionariesAPIDetails("lemmas");
            Response response = oxfordDictionariesAPI.getOxfordDictionariesAPI(oxfordDictionariesAPIDetails, inputWord.get(i));
            if (response.statusCode() == 200) {
                assert response.statusCode() == statusCode;
                ResponseBody responseBody = response.getBody();
                headWordId.add(i, responseBody.jsonPath().getString("results[0].lexicalEntries[0].inflectionOf[0].id")
                        .replaceAll("\\[", "").replaceAll("]", ""));
                Log.info("API response status for '"+ inputWord.get(i) + "' is: " + response.statusCode());
                Log.info("headword id for '" + inputWord.get(i) + "' is: '" + headWordId.get(i) + "'");
                Reporter.addStepLog("API response status for '"+ inputWord.get(i) + "' is: " + response.statusCode());
                Reporter.addStepLog("headword id for '" + inputWord.get(i) + "' is: '" + headWordId.get(i) + "'");
            } else {
                Log.error("cannot find word '" + inputWord.get(i) + "' in oxford dictionary");
                Reporter.addStepLog("cannot find the word '" + inputWord.get(i) + "' in oxford dictionary");
            }
        }
    }

    @Then("^the meaning of word \"([^\"]*)\" provided to the user$")
    public void theMeaningOfWordProvidedToTheUser(String str) {
        for (int i = 0; i < numberOfExecutions; i++) {
            if (str.equals("is")) {
                OxfordDictionariesAPIDetails oxfordDictionariesAPIDetails = new OxfordDictionariesAPIDetails("entries");
                Response response = oxfordDictionariesAPI.getOxfordDictionariesAPI(oxfordDictionariesAPIDetails, headWordId.get(i));
                ResponseBody responseBody = response.getBody();
                meaning.add(i, responseBody.jsonPath().getString("results[0].lexicalEntries[0].entries[0].senses[0].definitions")
                        .replaceAll("\\[", "").replaceAll("]", ""));
                Log.info("meaning of '" + inputWord.get(i) + "' is: '" + meaning.get(i) + "'");
                Reporter.addStepLog("meaning of '" + inputWord.get(i) + "' is: '" + meaning.get(i) + "'");
            }
            else{
                Log.info("cannot provide meaning of '" + inputWord.get(i) + "'");
                Reporter.addStepLog("cannot provide meaning of '" + inputWord.get(i) + "'");
            }
        }
    }

    @Given("^user provides (\\d+) \"([^\"]*)\" input$")
    public void userProvidesInput(int count, String sheetName) {
        numberOfExecutions = count;
        setExcelSheet(sheetName, (String) prop.get("testDataFilePath"));
        int maxRowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
        Random rand = new Random();
        for (int i=0; i<numberOfExecutions; i++){
            int randomRow = rand.nextInt((maxRowCount) + 1);
            inputWord.add(i, (getCellData(randomRow, 0)));
            Log.info("input word is: '" + inputWord.get(i) + "'");
            Reporter.addStepLog("input word is: '" + inputWord.get(i) + "'");
        }
    }
}
