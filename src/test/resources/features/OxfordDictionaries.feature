Feature: Find the meaning of a word

  As a Product Owner of Oxford Dictionary
  I want to get the meanings of English Words
  So that the user Retrieve dictionary information for a given word

  Scenario: Verify oxforddictionaries API returns meaning of the word for a valid input
    Given user provides 4 "valid" input
    When the oxforddictionaries API returns a response of 200
    Then the meaning of word "is" provided to the user

  Scenario: Verify oxforddictionaries API returns error for an invalid input
    Given user provides 2 "invalid" input
    When the oxforddictionaries API returns a response of 404
    Then the meaning of word "is not" provided to the user

