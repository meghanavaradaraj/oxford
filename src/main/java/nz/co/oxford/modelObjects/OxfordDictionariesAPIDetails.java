package nz.co.oxford.modelObjects;

import nz.co.oxford.helpers.Constants;
import static nz.co.oxford.helpers.Hooks.prop;

public class OxfordDictionariesAPIDetails {
    private String URI;
    private String endpoint;
    private String langCode;
    private String appKey;
    private String appId;

    public OxfordDictionariesAPIDetails(String basePath){
        setAppKey((String) prop.get("oxfordDictionaryAppKey"));
        setAppId((String) prop.get("oxfordDictionaryAppId"));
        setURI((String) prop.get("oxfordDictionaryURI"));
        if(basePath.equals("lemmas")) {
            setEndpoint(Constants.LEMMAS_ENDPOINT);
            setLangCode(Constants.LEMMAS_LANG_CODE);
        }
        else if (basePath.equals("entries")) {
            setEndpoint(Constants.ENTRIES_ENDPOINT);
            setLangCode(Constants.US_ENGLISH);
        }
    }

    public String getURI() {
        return URI;
    }

    public void setURI(String URI) {
        this.URI = URI;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

}
