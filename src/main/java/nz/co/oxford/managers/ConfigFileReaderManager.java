package nz.co.oxford.managers;

import java.io.*;
import java.net.URL;
import java.util.Properties;

public class ConfigFileReaderManager {

    public static Properties prop;
    static String propFileName = "/config/config.properties";

    public java.util.Properties loadParams() throws IOException {
        Properties prop = new Properties();
        URL propertiesFileURL = this.getClass().getResource(propFileName);
        try {
            prop.load(new FileInputStream(new File(propertiesFileURL.getPath())));
            return prop;
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("property file '" + propFileName + "' not found in the classpath");
        }

    }

}
