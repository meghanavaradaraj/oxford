package nz.co.oxford.helpers;

import com.cucumber.listener.Reporter;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import nz.co.oxford.managers.ConfigFileReaderManager;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class Hooks extends ConfigFileReaderManager{

    public static Log log = new Log();
    public static Properties prop;

    public Hooks() throws IOException {
        prop = loadParams();
    }

    @Before
    public void setUp(Scenario scenario) throws IOException {
        log.startTestCase(scenario.getName());
    }

    @After
    public static void writeExtentReport(Scenario scenario) throws IOException {
        String reportConfigPath =(String) prop.get("reportConfigPath");
        Reporter.loadXMLConfig(new File(reportConfigPath));
        log.endTestCase(scenario.getName());
    }

}
