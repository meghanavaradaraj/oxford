package nz.co.oxford.helpers;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.FileInputStream;
import java.io.IOException;

public class ExcelUtil {
    public static XSSFWorkbook workbook;
    public static XSSFSheet sheet;
    public static XSSFRow row;
    public static FileInputStream file;
    public static XSSFCell cell;
    public static String sheetName;

    public static void setExcelSheet(String excelSheetName, String filePath) {
        try{
            sheetName = excelSheetName;
            file = new FileInputStream(filePath);
            workbook = new XSSFWorkbook(file);
            sheet = workbook.getSheet(sheetName);
        } catch(IOException e){
            e.printStackTrace();
            Log.info("unable to set excel sheet");
        }
    }

    public static String getCellData(int row, int col) {
        cell = sheet.getRow(row).getCell(col);
        DataFormatter formatter = new DataFormatter();
        return formatter.formatCellValue(cell).trim();
    }
}
