package nz.co.oxford.helpers;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Log {

    private static Logger log;

    public Log() {
        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.INFO);
        log = Logger.getLogger(Log.class.getName());
    }

    public void startTestCase(String scenarioName){
        log.info("----------------------------------------------------------------------------------------");
        log.info("---------------------------------- Starting execution ----------------------------------");
        log.info("---------------------------- Scenario Name: "+scenarioName+ " --------------------------");
        log.info("----------------------------------------------------------------------------------------");
    }

    public void endTestCase(String scenarioName){
        log.info("----------------------------------------------------------------------------------------");
        log.info("--------------- Test execution completed for Scenario: "+scenarioName+ " ---------------");
        log.info("----------------------------------------------------------------------------------------");
    }

    public static void info(String message) {
        Logger.getRootLogger().setLevel(Level.INFO);
        log.info(message);
    }

    public static void warn(String message) {
        Logger.getRootLogger().setLevel(Level.WARN);
        log.warn(message);
    }

    public static void error(String message) {
        Logger.getRootLogger().setLevel(Level.ERROR);
        log.error(message);
    }

    public static void fatal(String message) {
        Logger.getRootLogger().setLevel(Level.FATAL);
        log.fatal(message);
    }

    public static void debug(String message) {
        Logger.getRootLogger().setLevel(Level.DEBUG);
        log.debug(message);
    }

}

