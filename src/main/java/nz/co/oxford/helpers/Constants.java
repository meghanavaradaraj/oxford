package nz.co.oxford.helpers;

public class Constants {
    public static String LEMMAS_ENDPOINT = "lemmas";
    public static String ENTRIES_ENDPOINT = "entries";
    public static String US_ENGLISH = "en-us";
    public static String LEMMAS_LANG_CODE = "en";
}
