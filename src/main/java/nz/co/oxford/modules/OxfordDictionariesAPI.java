package nz.co.oxford.modules;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import nz.co.oxford.helpers.Log;
import nz.co.oxford.modelObjects.OxfordDictionariesAPIDetails;

public class OxfordDictionariesAPI {

    Response response;

    public Response getOxfordDictionariesAPI(OxfordDictionariesAPIDetails APIDetails, String word) {
        RequestSpecification request = RestAssured.given();
        request.baseUri(APIDetails.getURI());
        request.basePath(APIDetails.getEndpoint()+"/"+APIDetails.getLangCode()+"/"+word);
        request.header("app_key", APIDetails.getAppKey());
        request.header("app_id", APIDetails.getAppId());
        try {
            response = request.get();
        } catch (AssertionError e) {
            Log.error("failed to get response from oxforddictionaries API for the endpoint: " + APIDetails.getEndpoint());
        }
        return response;
    }


}